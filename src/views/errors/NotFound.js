import React from "react";

function NotFound() {
  return (
    <div className="container">
      <div className="row justify-content-center align-items-lg-center vh-100">
        <div className="display-6">Page Not Found</div>
      </div>
    </div>
  );
}

export default NotFound;
